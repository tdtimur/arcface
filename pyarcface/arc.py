import torch

from .iresnet import iresnet100, IResNet


def load_arc_net() -> IResNet:
    arc_net = iresnet100(fp16=False)
    model = torch.load("backbone.pth", map_location=torch.device("cpu"))
    arc_net.load_state_dict(model)
    arc_net.eval()
    return arc_net
