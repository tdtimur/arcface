import io
import torch

from cog import BasePredictor, Input, File
from pyarcface.arc import load_arc_net


class Predictor(BasePredictor):
    def setup(self) -> None:
        self.model = load_arc_net()

    def predict(
        self,
        file: File = Input(description="Input tensor"),
    ) -> File:
        tensor = torch.load(file, map_location=torch.device('cpu'))
        result = self.model(tensor)
        buffer = io.BytesIO()
        torch.save(result, buffer)
        return buffer
